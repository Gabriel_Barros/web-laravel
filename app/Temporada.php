<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temporada extends Model
{
    protected $fillable = ['numero'];
    public $timestamps = false;

    public function episodios()
    {
        // Uma temporada tem Muitos episódios
        return $this->hasMany(Episodio::class);
    }

    public function serie()
    {
        // Uma temporada pertence à Uma série
        $this->belongsTo(Serie::class);
    }

    public function getEpisodiosAssistidos()
    {
        return $this->episodios->filter(function(Episodio $episodio) {
            return $episodio->assistido;
        });
    }
}
