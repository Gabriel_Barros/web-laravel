<?php


namespace App\Services;

use App\Episodio;
use App\Serie;
use App\Temporada;
use Illuminate\Support\Facades\DB;

class SerieCreate
{
    public function criarSerie(string $nomeSerie, int $qtdTemporadas, int $epPorTemporada): Serie
    {
        DB::beginTransaction();     //Inicia uma transação
            $serie = Serie::create(['nome' => $nomeSerie]);
            $this->adicionarTemporada($qtdTemporadas, $epPorTemporada, $serie);
        DB::commit();   //Fim da transação. Envia os dados dela para o Banco
        //
        return $serie;
    }

    public function adicionarTemporada(int $qtdTemporadas, int $epPorTemporada, Serie $serie)
    {
        for ($i = 1; $i <= $qtdTemporadas; $i++) {
            $temporada = $serie->temporadas()->create(['numero' => $i]);
            $this->adicionarEpisodios($epPorTemporada, $temporada);
        }
    }

    public function adicionarEpisodios(int $numEpisodios, Temporada $temporada)
    {
        for ($j = 1; $j <= $numEpisodios; $j++) {
            $temporada->episodios()->create(['numero' => $j]);
        }
    }
}
