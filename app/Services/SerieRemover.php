<?php


namespace App\Services;

use App\Episodio;
use App\Serie;
use App\Temporada;
use Illuminate\Support\Facades\DB;

class SerieRemover
{
    public function removerSerie(int $serieId): string
    {
        $nomeSerie = '';    //Passada vazia para que seja usada como referência

        //DB::transaction faz com que quando uma operação no Banco seja realizada, ela realmente só seja executada se a operação seja 100% concluída. Caso contrário, ele cancela a execução e nenhuma alteração é feita
        DB::transaction(function () use ($serieId, &$nomeSerie) {
            //Recebe uma função como parâmetro para que todos os procedimentos de exclusão estejam dentro do transaction
            $serie = Serie::find($serieId);
            $nomeSerie = $serie->nome;

            $this->removerTemporadas($serie);
            $serie->delete();
        });
        return $nomeSerie;
    }

    public function removerTemporadas(Serie $serie): void
    {
        $serie->temporadas->each(function (Temporada $temporada) {
           $this->removerEpisodios($temporada);
            $temporada->delete();
        });

    }

    private function removerEpisodios(Temporada $temporada)
    {
        $temporada->episodios()->each(function (Episodio $episodio) {
            $episodio->delete();
        });

    }
}
