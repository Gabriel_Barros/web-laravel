<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index');
    }

    public function login(Request $request)
    {
        //Attempt tenta realizar o login com os dados vindos do request(consultando no Banco) e retorna true ou false
        if(!Auth::attempt($request->only(['email', 'password']))) {
            return redirect()->back()->withErrors(
                'Usuário e/ou senha incorreto(s)'
            );
        }
        else
            return redirect()->route('show.series');
    }
}
