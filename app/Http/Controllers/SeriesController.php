<?php

namespace App\Http\Controllers;

use App\Http\Requests\SeriesFormRequest;
use App\Serie;
use App\Services\SerieCreate;
use App\Services\SerieRemover;
use Illuminate\Http\Request;

class SeriesController extends Controller {

    public function index(Request $request)
    {
        $series = Serie::query()->orderBy('nome')->get();  //Faz uma consulta no modelo de Banco, trazendo os nomes por ordem alfabética
        $mensagem = $request->session()->get('mensagem');     //Lê o que está vindo na sessão(dados do usuário que podem ser lidos em outras requisições)

        return view('series.index', compact('series', 'mensagem'));
    }

    public function create()
    {
        return view('series.create');
    }

    public function store(SeriesFormRequest $request, SerieCreate $serieCreate)
    {
        $serie = $serieCreate->criarSerie(      //Chamando o método do Service e criando todos os parâmetros que ele pede (referentes aos fillabels das Models)
            $request->nome,
            $request->qtd_temporadas,
            $request->ep_por_temporada
        );

        $request->session()->flash(         //Cria uma mensagem de aviso quando uma serie for criada
            'mensagem',
            "Série {$serie->nome} e suas temporadas foram adicionadas "
        );
        return redirect()->route('show.series');
    }

    public function destroy(Request $request, SerieRemover $serieRemover)
    {
        $nomeSerie = $serieRemover->removerSerie($request->id);

        $request->session()->flash(         //Cria uma mensagem de aviso, que dura somente durante aquela response, quando uma serie for criada
            'mensagem',
            "Série deletada com sucesso"
        );
        return redirect()->route('show.series');
    }

    public function editaNome(int $id, Request $request)
    {
        $novoNome = $request->nome;     //Recebe o nome que está vindo no corpo do formulário (função editarSerie() do index.php)
        $serie = Serie::find($id);     //Recupera o id da Serie
        $serie->nome = $novoNome;       //Substitui o nome atual da serie pelo nome passado na requisição
        $serie->save();
    }
}
