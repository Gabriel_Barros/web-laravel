<?php

namespace App\Http\Controllers;

use App\Episodio;
use App\Temporada;
use Illuminate\Http\Request;

class EpisodiosController extends Controller
{
    public function index(Temporada $temporada, Request $request)
    {
//        $episodios = $temporada->episodios;
//        $temporadaId = $temporada->id;
//        return view('episodios.index', compact('episodios', $temporadaId));   //É o mesmo que fazer:

         return view('episodios.index', [
            'episodios' => $temporada->episodios,
            'temporadaId' => $temporada->id,
             'mensagem' => $request->session()->get('mensagem')
        ]);
    }

    public function assistir(Temporada $temporada, Request $request)
    {
        $episodiosAssistidos = $request->episodios;     //Recebe os episodios(por ID) que estão vindo no request(episodios.index)
        //Faz um loop nos episodios
        $temporada->episodios()->each(function (Episodio $episodio) use ($episodiosAssistidos) {
            //Verifica se o episodioId está dentro dos episodios Assistidos. Se não estiver, o retorno da função vai ser False
            $episodio->assistido = in_array($episodio->id, $episodiosAssistidos);
        });
        $temporada->push();     //Envia todas as modificações
        $request->session()->flash('mensagem', 'Episódios marcados como assistido');

        return redirect()->back();
    }
}
