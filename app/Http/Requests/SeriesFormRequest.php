<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SeriesFormRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() //Forma do laravel de realizar validações, utilizando nomes pré-definidos para cada situação, de forma separada do Controller
    {
        return [
            'nome' => 'required|min:2'
        ];
    }

    public function messages()  //Função que cria menssagens personalizadas, recebendo as informações vindas da função rules()
    {
        return [
          'nome.required' => 'O campo :attribute é obrigatório',
            'nome.min' => 'O campo :attribute precisa ter no mínimo 2 caracteres'
        ];
    }

}
