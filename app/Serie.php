<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    public $timestamps = false;  // Desabilita o create_at e update_ate do database
    protected $fillable = ['nome']; //Especifica quais dados podem ser preenchidos no input de series

    public function temporadas()
    {
        // Uma série tem Muitas temporadas
        return $this->hasMany(Temporada::class);
    }
}
