<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('show.series');
});
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/series/criar', 'SeriesController@create')->name('series.criar');
    Route::post('/series/criar', 'SeriesController@store')->name('post.series');
    Route::delete('/series/remover/{id}', 'SeriesController@destroy')->name('delete.series');
    Route::post('series/{id}/editaNome', 'SeriesController@editaNome');
    Route::post('/temporada/{temporada}/episodios/assistir', 'EpisodiosController@assistir');
});

Route::get('/series', 'SeriesController@index')->name('show.series');
Route::get('/series/{serieId}/temporadas', 'TemporadasController@index');
Route::get('/temporadas/{temporada}/episodios', 'EpisodiosController@index');

Route::get('/login', 'LoginController@index')->name('show.login');
Route::post('/login', 'LoginController@login');
Route::get('/register', 'RegistroController@create');
Route::post('/register', 'RegistroController@store');
Route::get('/logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    return redirect()->route('show.login');
});
