<?php

namespace Tests\Unit;

use App\Services\SerieCreate;
use App\Services\SerieRemover;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RemovedorDeSerieTest extends TestCase
{
    use RefreshDatabase;
    private $serie;

    protected function setUp(): void
    {
        parent::setUp();
        $criadorDeSerie = new SerieCreate();
        $this->serie = $criadorDeSerie->criarSerie('Nome da Série', 1, 1);
    }

    public function testRemoverSerie()
    {
        $this->assertDatabaseHas('series', ['id' => $this->serie->id]);

        $removedorDeSerie = new SerieRemover();
        $nomeSerie = $removedorDeSerie->removerSerie($this->serie->id);

        $this->assertIsString($nomeSerie);
        $this->assertEquals('Nome da Série', $this->serie->nome);
        $this->assertDatabaseMissing('series', ['id' => $this->serie->id]);
    }
}
