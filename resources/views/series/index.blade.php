@extends('layout')

@section('cabecalho')
    Séries
@endsection

@section('conteudo')
    @include('mensagem', ['mensagem' => $mensagem])  {{--Vindo do mensagem.blade.php--}}

    @auth
    <a href="{{route('series.criar')}}" class="btn btn-dark mb-2">Adicionar</a>
    @endauth

    <ul class="list-group">
        @foreach ($series as $serie)
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <span id="nome-serie-{{ $serie->id }}">{{ $serie->nome }}</span>

                <div class="input-group w-50" hidden id="input-nome-serie-{{ $serie->id }}">
                    <input type="text" class="form-control" value="{{ $serie->nome }}">
                    <div class="input-group-append">
                        <button class="btn btn-primary" onclick="editarSerie({{ $serie->id }})">
                            <i class="fas fa-check"></i>
                        </button>
                        @csrf
                    </div>
                </div>

                <span class="d-flex">
                    @auth
                        <button class="btn btn-info btn-sm mr-1" onclick="toggleInput({{ $serie->id }})">
                            <i class="fas fa-edit"></i>
                        </button>
                    @endauth

                    <a href="/series/{{ $serie->id }}/temporadas" class="btn btn-info btn-sm mr-1">
                        <i class="fas fa-external-link-alt"></i>
                    </a>

                    @auth
                        <form method="post" action="/series/remover/{{ $serie->id }}" onsubmit="return confirm('Remover série {{addslashes($serie->nome)}}?' )">{{--Onsubmit exibe um input de confirmação antes de efetuar a remoção. Addslashes dá maior segurança ao passar a mensagem, para evitar quebrar --}}
                            @csrf
                            @method('DELETE')   {{--Faz com que o metodo delete, passado na rota, seja aceito--}}
                            <button class="btn btn-danger btn-sm">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </form>
                    @endauth
                </span>
            </li>
        @endforeach
    </ul>

    <script>
        function toggleInput(serieId) {     //Função que habilita a edição do nome da série

            const nomeSerieElement = document.getElementById(`nome-serie-${serieId}`);
            const inputSerieElement = document.getElementById(`input-nome-serie-${serieId}`);

            if(nomeSerieElement.hasAttribute('hidden')){    //Se o nome da serie estiver escondido,
                nomeSerieElement.removeAttribute('hidden'); //remove o atributo hidden do nome,
                inputSerieElement.hidden = true;            //esconde o campo input
            }
            else{                                               //Se o nome não está escondido
                inputSerieElement.removeAttribute('hidden');    //Exibe o input
                nomeSerieElement.hidden = true;                 //Esconde o nome
            }

        }

        function editarSerie(serieId) {
            let formData = new FormData();      //Novo formulário
            //Variavel nome recebe o valor do input da série
            const nome = document.querySelector(`#input-nome-serie-${serieId} > input`).value;

            //Recebe o valor de um input que tenha o name _token
            const token = document.querySelector('input[name="_token"]').value;

            formData.append('nome', nome);      //Adiciona o nome da série à um campo nome do formulário
            formData.append('_token', token);     //Adiciona o token do input(csrf) à um campo token do formulário

            const url = `/series/${serieId}/editaNome`;
            fetch(url, {        //Faz um request para uma url
               body: formData,
                method: 'POST'
            }).then(() => {
                toggleInput(serieId);
                document.getElementById(`nome-serie-${serieId}`).textContent = nome;
            });
        }
    </script>
@endsection

