@extends('layout')

@section('cabecalho')
    Episódios
@endsection

@section('conteudo')
   @include('mensagem', ['mensagem' => $mensagem])  {{--Vindo do mensagem.blade.php--}}

    <form action="/temporada/{{$temporadaId}}/episodios/assistir" method="POST">
        @csrf
        <ul class="list-group">
            @foreach ($episodios as $episodio)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Ep {{$episodio->numero}}
                    <input type="checkbox" name="episodios[]"
                           value="{{ $episodio->id }}"
                        {{-- Se o episodio estiver com o status assistido, marca a checkbox--}}
                        {{$episodio->assistido ? 'checked' : ''}}
                    >
                </li>
            @endforeach
        </ul>

        <button class="btn btn-primary mt-2 mb-2">Salvar</button>
    </form>
@endsection
